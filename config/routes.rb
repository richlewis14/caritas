CaritasMain::Application.routes.draw do

  root :to => 'mains#index'
  
  scope :controller => :mains do
    get 'about'

    match "our_projects/caritas" => "mains#caritas"

  end
    
  #CONTACT FORM
  match 'contact' => 'contact#new', :as => 'contact', :via => :get   
  match 'contact' => 'contact#create', :as => 'contact', :via => :post

  
end
